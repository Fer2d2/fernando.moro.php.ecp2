# PHP

## Práctica 2 - Symfony

### Datos del alumno

- **Nombre**: Fernando Moro Hernández
- **Matrícula**: BH0611
- **Curso**: FdS - Máster en Ingeniería Web

### Guía de uso

#### Entorno de desarrollo
Ejecutar el servidor de desarrollo con el siguiente comando:

```bash
php app/console server:start
```

Todas las rutas de la práctica se encuentran por debajo de `http://localhost:8000/api/`. Para obtener un listado de todas las rutas disponibles, utilizar el comando:

```bash
php app/console router:debug
```

#### Entorno de pruebas
La base de datos para el entorno de **test** se ha configurado en el fichero `app/config/config_test.yml`.

Se ha utilizado *DoctrineFixturesBundle* para generar una batería de datos de prueba. Para ejecutar los tests, previamente se deberán ejecutar los siguientes comandos:

##### Generación de base de datos de prueba:

```bash
php app/console doctrine:database:create --env=test
php app/console doctrine:schema:update --env=test --force
```

##### Carga de datos de prueba:

```bash
php app/console doctrine:fixtures:load --env=test
```

**Nota**: la ejecución del anterior comando implica el borrado del contenido de la base de datos de pruebas.

##### Ejecución de las pruebas:

```bash
vendor/phpunit/phpunit/phpunit -c app/
```