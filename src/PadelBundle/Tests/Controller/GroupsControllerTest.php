<?php

namespace PadelBundle\Tests\Controller;

use Liip\FunctionalTestBundle\Test\WebTestCase;

class GroupsControllerTest extends ApiWebTestCase
{
    public function setUp()
    {
        parent::setUp();
    }

    public function testCget()
    {
        $route = $this->getUrl('cget_groups', ['_format' => 'json']);
        $this->client->request('GET', $route);

        $response = $this->client->getResponse();
        $content = $response->getContent();

        $this->assertJson($content, 200);

        $courts = $this->getMainTestRepository()->findAll();
        $decoded = json_decode($content, true);

        $this->assertGreaterThan(0, count($decoded));
        $this->assertEquals(count($courts), count($decoded));
    }

    public function testGet()
    {
        $groups = $this->getMainTestRepository()->findAll();
        $group = $groups[0];

        $route = $this->getUrl('get_groups', ['slug' => $group->getId(), '_format' => 'json']);
        $this->client->request('GET', $route);

        $response = $this->client->getResponse();
        $content = $response->getContent();
        $decoded = json_decode($content, true);

        $this->assertJson($content, 200);

        $this->assertEquals($group->getId(), $decoded['id']);
    }

    public function testPost()
    {
        $group = [
            'name' => 'test_user',
            'roles' => ['ROLE_TESTER'],
        ];

        $encoded_user = json_encode($group);

        $route = $this->getUrl('post_groups', ['_format' => 'json']);
        $this->client->request('POST', $route, [], [], ['CONTENT_TYPE' => 'application/json'], $encoded_user);

        $response = $this->client->getResponse();
        $content = $response->getContent();
        $decoded = json_decode($content, true);

        $this->assertJson($content, 200);

        $this->assertArrayHasKey('id', $decoded);

        $this->assertEquals($group['name'], $decoded['name']);
        $this->assertEquals($group['roles'], $decoded['roles']);
    }

    public function testPut()
    {
        $groups = $this->getMainTestRepository()->findAll();
        $group = $groups[0];

        $user_array = [
            'name' => 'test_user',
            'roles' => ['ROLE_TESTER', 'ROLE_ADVANCED_TESTER'],
        ];
        $encoded_court = json_encode($user_array);

        $route = $this->getUrl('put_groups', ['slug' => $group->getId(), '_format' => 'json']);
        $this->client->request('PUT', $route, [], [], ['CONTENT_TYPE' => 'application/json'], $encoded_court);

        $response = $this->client->getResponse();
        $content = $response->getContent();
        $decoded = json_decode($content, true);

        $this->assertJson($content, 200);

        $this->assertArrayHasKey('id', $decoded);

        $this->assertEquals($user_array['name'], $decoded['name']);
        $this->assertEquals($user_array['roles'], $decoded['roles']);
    }

    public function testPutEmptyBody()
    {
        $groups = $this->getMainTestRepository()->findAll();
        $group = $groups[0];

        $route = $this->getUrl('put_groups', ['slug' => $group->getId(), '_format' => 'json']);
        $this->client->request('PUT', $route, [], [], ['CONTENT_TYPE' => 'application/json'], "{}");

        $response = $this->client->getResponse();
        $this->assertEquals(400, $response->getStatusCode());
    }

    public function testPatch()
    {
        $groups = $this->getMainTestRepository()->findAll();
        $group = $groups[0];

        $user_array = [
            'name' => 'test_user',
            'roles' => ['ROLE_TESTER', 'ROLE_ADVANCED_TESTER'],
        ];
        $encoded_court = json_encode($user_array);

        $route = $this->getUrl('patch_groups', ['slug' => $group->getId(), '_format' => 'json']);
        $this->client->request('PATCH', $route, [], [], ['CONTENT_TYPE' => 'application/json'], $encoded_court);

        $response = $this->client->getResponse();
        $content = $response->getContent();
        $decoded = json_decode($content, true);

        $this->assertJson($content, 200);

        $this->assertArrayHasKey('id', $decoded);

        $this->assertEquals($user_array['name'], $decoded['name']);
        $this->assertEquals($user_array['roles'], $decoded['roles']);
    }

    public function testPatchEmptyBody()
    {
        $groups = $this->getMainTestRepository()->findAll();
        $group = $groups[0];

        $route = $this->getUrl('patch_groups', ['slug' => $group->getId(), '_format' => 'json']);
        $this->client->request('PATCH', $route, [], [], ['CONTENT_TYPE' => 'application/json'], "{}");

        $response = $this->client->getResponse();
        $content = $response->getContent();
        $decoded = json_decode($content, true);

        $this->assertJson($content, 200);
        $this->assertEquals(200, $response->getStatusCode());

        $this->assertArrayHasKey('id', $decoded);
    }

    public function testDelete()
    {
        $groups = $this->getMainTestRepository()->findAll();
        $group = $groups[0];
        $groupsBeforeDeletion = count($groups);

        $route = $this->getUrl('delete_groups', ['slug' => $group->getId(), '_format' => 'json']);
        $this->client->request('DELETE', $route, [], [], ['CONTENT_TYPE' => 'application/json']);

        $response = $this->client->getResponse();
        $this->assertEquals(204, $response->getStatusCode());

        $groups = $this->getMainTestRepository()->findAll();
        $groupsAfterDeletion = count($groups);

        $this->assertEquals($groupsBeforeDeletion - 1, $groupsAfterDeletion);
    }

    public function testDeleteNonExistent()
    {
        $groups = $this->getMainTestRepository()->findAll();
        $groupsBeforeDeletion = count($groups);

        $route = $this->getUrl('delete_groups', ['slug' => rand(PHP_INT_MAX - 100, PHP_INT_MAX), '_format' => 'json']);
        $this->client->request('DELETE', $route, [], [], ['CONTENT_TYPE' => 'application/json']);

        $response = $this->client->getResponse();
        $this->assertEquals(204, $response->getStatusCode());

        $groups = $this->getMainTestRepository()->findAll();
        $groupsAfterDeletion = count($groups);

        $this->assertEquals($groupsBeforeDeletion, $groupsAfterDeletion);
    }

    private function getMainTestRepository()
    {
        return $this->em->getRepository('PadelBundle:Group');
    }
}
