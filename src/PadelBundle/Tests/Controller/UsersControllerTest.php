<?php

namespace PadelBundle\Tests\Controller;

class UsersControllerTest extends ApiWebTestCase
{
    use AvailableDataSelector;

    public function setUp()
    {
        parent::setUp();
    }

    public function testCget()
    {
        $route = $this->getUrl('cget_users', ['_format' => 'json']);
        $this->client->request('GET', $route);

        $response = $this->client->getResponse();
        $content = $response->getContent();

        $this->assertJson($content, 200);

        $courts = $this->getMainTestRepository()->findAll();
        $decoded = json_decode($content, true);

        $this->assertGreaterThan(0, count($decoded));
        $this->assertEquals(count($courts), count($decoded));
    }

    public function testGet()
    {
        $users = $this->getMainTestRepository()->findAll();
        $user = $users[0];

        $route = $this->getUrl('get_users', ['slug' => $user->getId(), '_format' => 'json']);
        $this->client->request('GET', $route);

        $response = $this->client->getResponse();
        $content = $response->getContent();
        $decoded = json_decode($content, true);

        $this->assertJson($content, 200);

        $this->assertEquals($user->getId(), $decoded['id']);
    }

    public function testPost()
    {
        $user = [
            'username' => 'test_user',
            'email' => 'test_user@php.net',
            'password' => 'super_secret_pass',
            'roles' => ['ROLE_TEST'],
            'enabled' => true,
            'locked' => true,
            'expired' => true,
        ];

        $encoded_user = json_encode($user);

        $route = $this->getUrl('post_users', ['_format' => 'json']);
        $this->client->request('POST', $route, [], [], ['CONTENT_TYPE' => 'application/json'], $encoded_user);

        $response = $this->client->getResponse();
        $content = $response->getContent();
        $decoded = json_decode($content, true);

        $this->assertJson($content, 200);

        $this->assertArrayHasKey('id', $decoded);

        $this->assertEquals($user['username'], $decoded['username']);
        $this->assertEquals($user['email'], $decoded['email']);
        $this->assertArrayNotHasKey('password', $decoded);
        $this->assertEquals($user['roles'], $decoded['roles']);
        $this->assertEquals($user['enabled'], $decoded['enabled']);
        $this->assertEquals($user['locked'], $decoded['locked']);
        $this->assertEquals($user['expired'], $decoded['expired']);

    }

    public function testPut()
    {
        $users = $this->getMainTestRepository()->findAll();
        $user = $users[0];

        $user_array = [
            'username' => 'test_user_2',
            'email' => 'test_user_2@php.net',
            'password' => 'super_secret_pass',
            'roles' => ['ROLE_TEST', 'ROLE_TESTER_PROMOTED'],
            'enabled' => true,
            'locked' => true,
            'expired' => true,
        ];
        $encoded_court = json_encode($user_array);

        $route = $this->getUrl('put_users', ['slug' => $user->getId(), '_format' => 'json']);
        $this->client->request('PUT', $route, [], [], ['CONTENT_TYPE' => 'application/json'], $encoded_court);

        $response = $this->client->getResponse();
        $content = $response->getContent();
        $decoded = json_decode($content, true);

        $this->assertJson($content, 200);

        $this->assertArrayHasKey('id', $decoded);

        $this->assertEquals($user_array['username'], $decoded['username']);
        $this->assertEquals($user_array['email'], $decoded['email']);
        $this->assertArrayNotHasKey('password', $decoded);
        $this->assertEquals($user_array['roles'], $decoded['roles']);
        $this->assertEquals($user_array['enabled'], $decoded['enabled']);
        $this->assertEquals($user_array['locked'], $decoded['locked']);
        $this->assertEquals($user_array['expired'], $decoded['expired']);
    }

    public function testPutEmptyBody()
    {
        $users = $this->getMainTestRepository()->findAll();
        $user = $users[0];

        $route = $this->getUrl('put_users', ['slug' => $user->getId(), '_format' => 'json']);
        $this->client->request('PUT', $route, [], [], ['CONTENT_TYPE' => 'application/json'], "{}");

        $response = $this->client->getResponse();
        $this->assertEquals(400, $response->getStatusCode());
    }

    public function testPatch()
    {
        $users = $this->getMainTestRepository()->findAll();
        $user = $users[0];

        $user_array = [
            'username' => 'test_user_3',
            'email' => 'test_user_3@php.net',
            'password' => 'super_secret_pass',
            'roles' => ['ROLE_TEST', 'ROLE_TESTER_PROMOTED'],
            'enabled' => true,
            'locked' => true,
            'expired' => true,
        ];
        $encoded_court = json_encode($user_array);

        $route = $this->getUrl('patch_users', ['slug' => $user->getId(), '_format' => 'json']);
        $this->client->request('PATCH', $route, [], [], ['CONTENT_TYPE' => 'application/json'], $encoded_court);

        $response = $this->client->getResponse();
        $content = $response->getContent();
        $decoded = json_decode($content, true);

        $this->assertJson($content, 200);

        $this->assertArrayHasKey('id', $decoded);

        $this->assertEquals($user_array['username'], $decoded['username']);
        $this->assertEquals($user_array['email'], $decoded['email']);
        $this->assertArrayNotHasKey('password', $decoded);
        $this->assertEquals($user_array['roles'], $decoded['roles']);
        $this->assertEquals($user_array['enabled'], $decoded['enabled']);
        $this->assertEquals($user_array['locked'], $decoded['locked']);
        $this->assertEquals($user_array['expired'], $decoded['expired']);
    }

    public function testPatchEmptyBody()
    {
        $users = $this->getMainTestRepository()->findAll();
        $user = $users[0];

        $route = $this->getUrl('patch_users', ['slug' => $user->getId(), '_format' => 'json']);
        $this->client->request('PATCH', $route, [], [], ['CONTENT_TYPE' => 'application/json'], "{}");

        $response = $this->client->getResponse();
        $content = $response->getContent();
        $decoded = json_decode($content, true);

        $this->assertJson($content, 200);
        $this->assertEquals(200, $response->getStatusCode());

        $this->assertArrayHasKey('id', $decoded);
    }

    public function testDelete()
    {
        $users = $this->getMainTestRepository()->findAll();
        $user = $this->getAvailableUser($this->em);
        $usersBeforeDeletion = count($users);

        $route = $this->getUrl('delete_users', ['slug' => $user->getId(), '_format' => 'json']);
        $this->client->request('DELETE', $route, [], [], ['CONTENT_TYPE' => 'application/json']);

        $response = $this->client->getResponse();
        $this->assertEquals(204, $response->getStatusCode());

        $users = $this->getMainTestRepository()->findAll();
        $usersAfterDeletion = count($users);

        $this->assertEquals($usersBeforeDeletion - 1, $usersAfterDeletion);
    }

    public function testDeleteNonExistent()
    {
        $users = $this->getMainTestRepository()->findAll();
        $usersBeforeDeletion = count($users);

        $route = $this->getUrl('delete_users', ['slug' => rand(PHP_INT_MAX - 100, PHP_INT_MAX), '_format' => 'json']);
        $this->client->request('DELETE', $route, [], [], ['CONTENT_TYPE' => 'application/json']);

        $response = $this->client->getResponse();
        $this->assertEquals(204, $response->getStatusCode());

        $users = $this->getMainTestRepository()->findAll();
        $usersAfterDeletion = count($users);

        $this->assertEquals($usersBeforeDeletion, $usersAfterDeletion);
    }

    private function getMainTestRepository()
    {
        return $this->em->getRepository('PadelBundle:User');
    }
}
