<?php

namespace PadelBundle\Tests\Controller;

class CourtsControllerTest extends ApiWebTestCase
{
    use AvailableDataSelector;

    public function setUp()
    {
        parent::setUp();
    }

    public function testCget()
    {
        $route = $this->getUrl('cget_courts', ['_format' => 'json']);
        $this->client->request('GET', $route);

        $response = $this->client->getResponse();
        $content = $response->getContent();

        $this->assertJson($content, 200);

        $courts = $this->getMainTestRepository()->findAll();
        $decoded = json_decode($content, true);

        $this->assertEquals(count($courts), count($decoded));
    }

    public function testGet()
    {
        $courts = $this->getMainTestRepository()->findAll();
        $court = $courts[0];

        $route = $this->getUrl('get_courts', ['slug' => $court->getId(), '_format' => 'json']);
        $this->client->request('GET', $route);

        $response = $this->client->getResponse();
        $content = $response->getContent();
        $decoded = json_decode($content, true);

        $this->assertJson($content, 200);

        $this->assertEquals($court->getId(), $decoded['id']);
    }

    public function testPostWithActiveFalse()
    {
        $court = [
            'active' => false
        ];

        $encoded_court = json_encode($court);

        $route = $this->getUrl('post_courts', ['_format' => 'json']);
        $this->client->request('POST', $route, [], [], ['CONTENT_TYPE' => 'application/json'], $encoded_court);

        $response = $this->client->getResponse();
        $content = $response->getContent();
        $decoded = json_decode($content, true);

        $this->assertJson($content, 200);
        $this->assertArrayHasKey('id', $decoded);
        $this->assertEquals($court['active'], $decoded['active']);
    }

    public function testPostWithActiveTrue()
    {
        $court = [
            'active' => false
        ];

        $encoded_court = json_encode($court);

        $route = $this->getUrl('post_courts', ['_format' => 'json']);
        $this->client->request('POST', $route, [], [], ['CONTENT_TYPE' => 'application/json'], $encoded_court);

        $response = $this->client->getResponse();
        $content = $response->getContent();
        $decoded = json_decode($content, true);

        $this->assertJson($content, 200);
        $this->assertArrayHasKey('id', $decoded);
        $this->assertEquals($court['active'], $decoded['active']);
    }

    public function testPut()
    {
        $courts = $this->getMainTestRepository()->findAll();
        $court = $courts[0];
        $active = $court->getActive() ? false : true;
        $court->setActive($active);

        $court_array = [
            'id' => $court->getId(),
            'active' => $court->getActive(),
        ];
        $encoded_court = json_encode($court_array);

        $route = $this->getUrl('put_courts', ['slug' => $court->getId(), '_format' => 'json']);
        $this->client->request('PUT', $route, [], [], ['CONTENT_TYPE' => 'application/json'], $encoded_court);

        $response = $this->client->getResponse();
        $content = $response->getContent();
        $decoded = json_decode($content, true);

        $this->assertJson($content, 200);
        $this->assertEquals($active, $decoded['active']);
    }

    public function testPutEmptyBody()
    {
        $courts = $this->getMainTestRepository()->findAll();
        $court = $courts[0];

        $route = $this->getUrl('put_courts', ['slug' => $court->getId(), '_format' => 'json']);
        $this->client->request('PUT', $route, [], [], ['CONTENT_TYPE' => 'application/json'], "{}");

        $response = $this->client->getResponse();
        $this->assertEquals(400, $response->getStatusCode());
    }

    public function testPatch()
    {
        $courts = $this->getMainTestRepository()->findAll();
        $court = $courts[0];
        $active = $court->getActive() ? false : true;
        $court->setActive($active);

        $court_array = [
            'id' => $court->getId(),
            'active' => $court->getActive(),
        ];
        $encoded_court = json_encode($court_array);

        $route = $this->getUrl('patch_courts', ['slug' => $court->getId(), '_format' => 'json']);
        $this->client->request('PATCH', $route, [], [], ['CONTENT_TYPE' => 'application/json'], $encoded_court);

        $response = $this->client->getResponse();
        $content = $response->getContent();
        $decoded = json_decode($content, true);

        $this->assertJson($content, 200);
        $this->assertEquals(200, $response->getStatusCode());

        $this->assertArrayHasKey('id', $decoded);
    }

    public function testPatchEmptyBody()
    {
        $courts = $this->getMainTestRepository()->findAll();
        $court = $courts[0];

        $route = $this->getUrl('put_courts', ['slug' => $court->getId(), '_format' => 'json']);
        $this->client->request('PUT', $route, [], [], ['CONTENT_TYPE' => 'application/json'], "{}");

        $response = $this->client->getResponse();
        $this->assertEquals(400, $response->getStatusCode());
    }

    public function testDelete()
    {
        $courts = $this->getMainTestRepository()->findAll();
        $court = $this->getAvailableCourt($this->em);
        $courtsBeforeDeletion = count($courts);

        $route = $this->getUrl('delete_courts', ['slug' => $court->getId(), '_format' => 'json']);
        $this->client->request('DELETE', $route, [], [], ['CONTENT_TYPE' => 'application/json']);

        $response = $this->client->getResponse();
        $this->assertEquals(204, $response->getStatusCode());

        $courts = $this->getMainTestRepository()->findAll();
        $courtsAfterDeletion = count($courts);

        $this->assertEquals($courtsBeforeDeletion - 1, $courtsAfterDeletion);
    }

    public function testDeleteNonExistent()
    {
        $courts = $this->getMainTestRepository()->findAll();
        $courtsBeforeDeletion = count($courts);

        $route = $this->getUrl('delete_courts', ['slug' => rand(PHP_INT_MAX - 100, PHP_INT_MAX), '_format' => 'json']);
        $this->client->request('DELETE', $route, [], [], ['CONTENT_TYPE' => 'application/json']);

        $response = $this->client->getResponse();
        $this->assertEquals(204, $response->getStatusCode());

        $courts = $this->getMainTestRepository()->findAll();
        $courtsAfterDeletion = count($courts);

        $this->assertEquals($courtsBeforeDeletion, $courtsAfterDeletion);
    }

    private function getMainTestRepository()
    {
        return $this->em->getRepository('PadelBundle:Court');
    }
}
