<?php

namespace PadelBundle\Tests\Controller;

class ReservationsControllerTest extends ApiWebTestCase
{
    use AvailableDataSelector;

    public function setUp()
    {
        parent::setUp();
    }

    public function testCget()
    {
        $route = $this->getUrl('cget_reservations', ['_format' => 'json']);
        $this->client->request('GET', $route);

        $response = $this->client->getResponse();
        $content = $response->getContent();

        $this->assertJson($content, 200);

        $courts = $this->getMainTestRepository()->findAll();
        $decoded = json_decode($content, true);

        $this->assertGreaterThan(0, count($decoded));
        $this->assertEquals(count($courts), count($decoded));
    }

    public function testGet()
    {
        $reservations = $this->getMainTestRepository()->findAll();
        $reservation = $reservations[0];

        $route = $this->getUrl('get_reservations', ['slug' => $reservation->getId(), '_format' => 'json']);
        $this->client->request('GET', $route);

        $response = $this->client->getResponse();
        $content = $response->getContent();
        $decoded = json_decode($content, true);

        $this->assertJson($content, 200);

        $this->assertEquals($reservation->getId(), $decoded['id']);
    }

    public function testPost()
    {
        $reservation = [
            'datetime' => '2016-02-08 10:30:00',
            'user' => $this->getAvailableUser($this->em)->getId(),
            'court' => $this->getAvailableCourt($this->em)->getId(),
        ];

        $encoded_reservation = json_encode($reservation);

        $route = $this->getUrl('post_reservations', ['_format' => 'json']);
        $this->client->request('POST', $route, [], [], ['CONTENT_TYPE' => 'application/json'], $encoded_reservation);

        $response = $this->client->getResponse();
        $content = $response->getContent();
        $decoded = json_decode($content, true);

        $this->assertJson($content, 200);

        $this->assertArrayHasKey('id', $decoded);
        $this->assertArrayHasKey('user', $decoded);
        $this->assertArrayHasKey('court', $decoded);

    }

    public function testDelete()
    {
        $reservations = $this->getMainTestRepository()->findAll();
        $reservation = $reservations[0];
        $reservationsBeforeDeletion = count($reservations);

        $route = $this->getUrl('delete_reservations', ['slug' => $reservation->getId(), '_format' => 'json']);
        $this->client->request('DELETE', $route, [], [], ['CONTENT_TYPE' => 'application/json']);

        $response = $this->client->getResponse();
        $this->assertEquals(204, $response->getStatusCode());

        $reservations = $this->getMainTestRepository()->findAll();
        $reservationsAfterDeletion = count($reservations);

        $this->assertEquals($reservationsBeforeDeletion - 1, $reservationsAfterDeletion);
    }

    public function testDeleteNonExistent()
    {
        $reservations = $this->getMainTestRepository()->findAll();
        $reservationsBeforeDeletion = count($reservations);

        $route = $this->getUrl('delete_reservations', ['slug' => rand(PHP_INT_MAX - 100, PHP_INT_MAX), '_format' => 'json']);
        $this->client->request('DELETE', $route, [], [], ['CONTENT_TYPE' => 'application/json']);

        $response = $this->client->getResponse();
        $this->assertEquals(204, $response->getStatusCode());

        $reservations = $this->getMainTestRepository()->findAll();
        $reservationsAfterDeletion = count($reservations);

        $this->assertEquals($reservationsBeforeDeletion, $reservationsAfterDeletion);
    }

    private function getMainTestRepository()
    {
        return $this->em->getRepository('PadelBundle:Reservation');
    }
}
