<?php
namespace PadelBundle\Tests\Controller;

trait AvailableDataSelector
{
    public function getAvailableUser($em)
    {
        $reservations = $em->getRepository('PadelBundle:Reservation')->findAll();
        $users = $em->getRepository('PadelBundle:User')->findAll();

        $availableUsers = array_filter($users, function ($currentUser) use ($reservations) {
            foreach ($reservations as $reservation) {
                if ($reservation->getUser()->getId() === $currentUser->getId()) {
                    return false;
                }
            }
            return true;
        });

        return reset($availableUsers);
    }

    public function getAvailableCourt($em)
    {
        $reservations = $em->getRepository('PadelBundle:Reservation')->findAll();
        $courts = $em->getRepository('PadelBundle:Court')->findAll();

        $availableCourts = array_filter($courts, function ($currentCourt) use ($reservations) {
            foreach ($reservations as $reservation) {
                if ($reservation->getCourt()->getId() === $currentCourt->getId()) {
                    return false;
                }
            }
            return true;
        });

        return reset($availableCourts);
    }
}