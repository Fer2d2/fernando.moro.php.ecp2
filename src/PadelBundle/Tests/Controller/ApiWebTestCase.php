<?php

namespace PadelBundle\Tests\Controller;

use Liip\FunctionalTestBundle\Test\WebTestCase;

abstract class ApiWebTestCase extends WebTestCase
{
    protected $client;
    protected $em;

    public function setUp()
    {
        parent::setUp();

        $this->em = $this->getContainer()->get('doctrine')->getManager();
        $this->client = static::createClient();
        $this->loadCustomFixtures();
    }

    protected function loadCustomFixtures()
    {
        $fixtures = [
            'PadelBundle\DataFixtures\ORM\LoadCourtData',
            'PadelBundle\DataFixtures\ORM\LoadGroupData',
            'PadelBundle\DataFixtures\ORM\LoadUserData',
            'PadelBundle\DataFixtures\ORM\LoadReservationData',
        ];
        $this->loadFixtures($fixtures);
    }

}