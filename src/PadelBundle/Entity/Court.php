<?php

namespace PadelBundle\Entity;

use JMS\Serializer\Annotation\Groups;

/**
 * Court
 */
class Court
{
    /**
     * @var integer
     * @Groups({"detail_court"})
     */
    private $id;

    /**
     * @var boolean
     * @Groups({"detail_court"})
     */
    private $active = '1';


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set active
     *
     * @param boolean $active
     *
     * @return Court
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean
     */
    public function getActive()
    {
        return $this->active;
    }
}

