<?php

namespace PadelBundle\Entity;

use PadelBundle\Entity\User;
use JMS\Serializer\Annotation\Groups;

/**
 * Group
 */
class Group
{
    /**
     * @var integer
     * @Groups({"detail_group", "summary_group"})
     */
    private $id;

    /**
     * @var string
     * @Groups({"detail_group", "summary_group"})
     */
    private $name;

    /**
     * @var array
     * @Groups({"detail_group", "summary_group"})
     */
    private $roles;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @Groups({"detail_group"})
     */
    private $user;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->user = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Group
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set roles
     *
     * @param array $roles
     *
     * @return Group
     */
    public function setRoles($roles)
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * Get roles
     *
     * @return array
     */
    public function getRoles()
    {
        return $this->roles;
    }

    /**
     * Add user
     *
     * @param \PadelBundle\Entity\User $user
     *
     * @return Group
     */
    public function addUser(User $user)
    {
        $this->user[] = $user;
        $user->addGroup($this);
        return $this;
    }

    /**
     * Remove user
     *
     * @param \PadelBundle\Entity\User $user
     */
    public function removeUser(User $user)
    {
        $this->user->removeElement($user);
        $user->removeGroup($this);
    }

    /**
     * Get user
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUser()
    {
        return $this->user;
    }
}

