<?php

namespace PadelBundle\Entity;

use PadelBundle\Entity\Court;
use PadelBundle\Entity\User;
use JMS\Serializer\Annotation\Groups;

/**
 * Reservation
 */
class Reservation
{
    /**
     * @var integer
     * @Groups({"detail_reservation"})
     */
    private $id;

    /**
     * @var \DateTime
     * @Groups({"detail_reservation"})
     */
    private $datetime;

    /**
     * @var Court
     * @Groups({"detail_reservation"})
     */
    private $court;

    /**
     * @var User
     * @Groups({"detail_reservation"})
     */
    private $user;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set datetime
     *
     * @param \DateTime $datetime
     *
     * @return Reservation
     */
    public function setDatetime($datetime)
    {
        $this->datetime = $datetime;

        return $this;
    }

    /**
     * Get datetime
     *
     * @return \DateTime
     */
    public function getDatetime()
    {
        return $this->datetime;
    }

    /**
     * Set court
     *
     * @param \PadelBundle\Entity\Court $court
     *
     * @return Reservation
     */
    public function setCourt(Court $court = null)
    {
        $this->court = $court;

        return $this;
    }

    /**
     * Get court
     *
     * @return Court
     */
    public function getCourt()
    {
        return $this->court;
    }

    /**
     * Set user
     *
     * @param User $user
     *
     * @return Reservation
     */
    public function setUser(User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \PadelBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }
}

