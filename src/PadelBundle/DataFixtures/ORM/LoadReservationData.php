<?php

namespace PadelBundle\DataFixtures\ORM;


use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use PadelBundle\Entity\Reservation;

class LoadReservationData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $users = $manager->getRepository('PadelBundle:User')->findAll();
        $courts = $manager->getRepository('PadelBundle:Court')->findAll();

        $reservation = new Reservation();
        $reservation
            ->setDatetime(new \DateTime())
            ->setCourt($courts[0])
            ->setUser($users[0]);

        $manager->persist($reservation);
        $manager->flush();
    }

    public function getOrder()
    {
        return 4;
    }
}