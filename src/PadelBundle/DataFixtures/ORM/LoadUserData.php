<?php

namespace PadelBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use PadelBundle\Entity\User;

class LoadUserData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $manager->persist($this->generateAdminManagerUser());
        $manager->persist($this->generateSuperAdminUser());
        $manager->persist($this->generatePlayerUser());
        $manager->flush();
    }

    public function getOrder()
    {
        return 2;
    }

    private function generateAdminManagerUser()
    {
        $user = new User();
        $user->setUsername('fernando')
            ->setUsernameCanonical('fernando')
            ->setEmail('fernando.moro@php.net')
            ->setEmailCanonical('fernando.moro@php.net')
            ->setEnabled(true)
            ->setSalt('jk7dsf987fsd98798sd789fd798s')
            ->setPassword('superSecret')
            ->setRoles(['ROLE_ADMIN', 'ROLE_MANAGER'])
            ->setLocked(false)
            ->setExpired(false)
            ->setCredentialsExpired(false);
        return $user;
    }

    private function generateSuperAdminUser()
    {
        $user = new User();
        $user->setUsername('jorge')
            ->setUsernameCanonical('jorge')
            ->setEmail('jorge.hernan@php.net')
            ->setEmailCanonical('jorge.hernan@php.net')
            ->setEnabled(true)
            ->setSalt('jk7dsf987fsd98798sd789fd798s')
            ->setPassword('superSecret')
            ->setRoles(['ROLE_SUPER_ADMIN'])
            ->setLocked(false)
            ->setExpired(false)
            ->setCredentialsExpired(false);
        return $user;
    }

    private function generatePlayerUser()
    {
        $user = new User();
        $user->setUsername('alvaro')
            ->setUsernameCanonical('alvaro')
            ->setEmail('alvaro.martin@php.net')
            ->setEmailCanonical('alvaro.martin@php.net')
            ->setEnabled(true)
            ->setSalt('jk7dsf987fsd98798sd789fd798s')
            ->setPassword('superSecret')
            ->setRoles(['ROLE_PLAYER', 'ROLE_USER'])
            ->setLocked(false)
            ->setExpired(false)
            ->setCredentialsExpired(false);
        return $user;
    }
}