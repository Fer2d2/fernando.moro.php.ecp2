<?php

namespace PadelBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use PadelBundle\Entity\Court;

class LoadCourtData extends AbstractFixture implements OrderedFixtureInterface
{
    const MAX_COURTS = 15;

    public function load(ObjectManager $manager)
    {
        for ($i = 0; $i < $this::MAX_COURTS; $i++) {
            $active = rand(0, 1);
            $court = new Court();
            $court->setActive($active);
            $manager->persist($court);
        }

        $manager->flush();
    }

    public function getOrder()
    {
        return 1;
    }
}