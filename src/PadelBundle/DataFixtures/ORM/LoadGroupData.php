<?php

namespace PadelBundle\DataFixtures\ORM;


use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use PadelBundle\Entity\Group;

class LoadGroupData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $manager->persist($this->generatePlayersGroup());
        $manager->persist($this->generateBackofficeUsersGroup());
        $manager->flush();
    }

    public function getOrder()
    {
        return 3;
    }

    private function generateBackofficeUsersGroup()
    {
        $group = new Group();
        $group->setName('backoffice')
            ->setRoles(['ROLE_MANAGER', 'ROLE_BACKOFFICE']);
        return $group;
    }

    private function generatePlayersGroup()
    {
        $group = new Group();
        $group->setName('players')
            ->setRoles(['ROLE_PLAYER']);
        return $group;
    }
}