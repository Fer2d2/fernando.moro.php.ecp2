<?php

namespace PadelBundle\Handler;

use FOS\RestBundle\View\ExceptionWrapperHandlerInterface;

class CustomMessageExceptionWrapperHandler implements ExceptionWrapperHandlerInterface
{
    public function wrap($data)
    {
        $exception = $data['exception'];

        $newException = [
            'code' => $data['status_code'],
            'message' => $exception->getMessage()
        ];

        return $newException;
    }
}