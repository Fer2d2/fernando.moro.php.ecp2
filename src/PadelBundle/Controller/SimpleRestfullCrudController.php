<?php

namespace PadelBundle\Controller;


use Symfony\Component\HttpFoundation\Request;

interface SimpleRestfullCrudController
{
    public function cgetAction();

    public function getAction($slug);

    public function postAction(Request $request);

    public function putAction($slug, Request $request);

    public function patchAction($slug, Request $request);

    public function deleteAction($slug);
}