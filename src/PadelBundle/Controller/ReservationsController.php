<?php

namespace PadelBundle\Controller;

use PadelBundle\Entity\Reservation;
use PadelBundle\Utils\ApiMessages;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use FOS\RestBundle\Util\Codes;
use FOS\RestBundle\Controller\Annotations\View;
use FOS\RestBundle\Controller\Annotations\RouteResource;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\HttpException;

/**
 * @RouteResource("Reservations")
 */
class ReservationsController extends Controller
{
    /**
     * @View(serializerGroups={"detail_reservation", "detail_court", "summary_user"})
     */
    public function cgetAction()
    {
        $em = $this->get('doctrine')->getEntityManager();
        $reservations = $this->getMainEntityRepository($em)->findAll();

        return $reservations;
    }

    /**
     * @View(serializerGroups={"detail_reservation", "detail_court", "summary_user"})
     */
    public function getAction($slug)
    {
        $em = $this->get('doctrine')->getEntityManager();
        $reservation = $this->getMainEntityRepository($em)->find($slug);

        return $reservation;
    }

    /**
     * @View(serializerGroups={"detail_reservation", "detail_court", "summary_user"})
     */
    public function postAction(Request $request)
    {
        $em = $this->get('doctrine')->getEntityManager();
        $paramUpdater = $this->get('api.param_updater');

        $reservation = new Reservation();

        $datetimeStr = $request->request->get('datetime');
        $datetime = \DateTime::createFromFormat("Y-m-d H:i:s", $datetimeStr);

        $courtId = $request->request->get('court');
        $court = $em->getRepository('PadelBundle:Court')->find($courtId);

        $userId = $request->request->get('user');
        $user = $em->getRepository('PadelBundle:User')->find($userId);

        $paramUpdater
            ->setEntity($reservation)
            ->update('datetime', $datetime, true)
            ->update('court', $court, true)
            ->update('user', $user, true);

        $em->persist($reservation);

        try {
            $em->flush();
        } catch (\Exception $e) {
            throw new HttpException(400, ApiMessages::ERROR_STORING_VALUE);
        }

        return $reservation;
    }

    /**
     * @View(serializerGroups={"detail_reservation", "detail_court", "summary_user"})
     */
    public function putAction($slug, Request $request)
    {
        $em = $this->get('doctrine')->getEntityManager();
        $paramUpdater = $this->get('api.param_updater');

        $reservation = $this->getMainEntityRepository($em)->find($slug);

        if (empty($reservation)) {
            return new JsonResponse('', Codes::HTTP_NO_CONTENT);
        }

        $datetimeStr = $request->request->get('datetime');
        $datetime = \DateTime::createFromFormat("Y-m-d H:i:s", $datetimeStr);

        $courtId = $request->request->get('court');
        $court = $em->getRepository('PadelBundle:Court')->find($courtId);

        $userId = $request->request->get('user');
        $user = $em->getRepository('PadelBundle:User')->find($userId);

        $paramUpdater
            ->setEntity($reservation)
            ->update('datetime', $datetime, true)
            ->update('court', $court, true)
            ->update('user', $user, true);

        $em->persist($reservation);

        try {
            $em->flush();
        } catch (\Exception $e) {
            return new HttpException(400, ApiMessages::ERROR_STORING_VALUE);
        }

        return $reservation;
    }

    /**
     * @View(serializerGroups={"detail_reservation", "detail_court", "summary_user"})
     */
    public function patchAction($slug, Request $request)
    {
        $em = $this->get('doctrine')->getEntityManager();
        $paramUpdater = $this->get('api.param_updater');
        $reservation = $this->getMainEntityRepository($em)->find($slug);

        if (empty($reservation)) {
            return new JsonResponse('', Codes::HTTP_NO_CONTENT);
        }

        $datetimeStr = $request->request->get('datetime');
        $datetime = \DateTime::createFromFormat("Y-m-d H:i:s", $datetimeStr);

        $courtId = $request->request->get('court');
        $court = $em->getRepository('PadelBundle:Court')->find($courtId);

        $userId = $request->request->get('user');
        $user = $em->getRepository('PadelBundle:User')->find($userId);

        $paramUpdater
            ->setEntity($reservation)
            ->update('datetime', $datetime, false)
            ->update('court', $court, false)
            ->update('user', $user, false);

        $em->persist($reservation);

        try {
            $em->flush();
        } catch (\Exception $e) {
            return new HttpException(400, ApiMessages::ERROR_STORING_VALUE);
        }

        return $reservation;
    }

    /**
     * @View()
     */
    public function deleteAction($slug)
    {
        $em = $this->get('doctrine')->getEntityManager();
        $reservation = $this->getMainEntityRepository($em)->find($slug);

        if (empty($reservation)) {
            return new JsonResponse('', Codes::HTTP_NO_CONTENT);
        }

        $em->remove($reservation);
        $em->flush();

        return new JsonResponse('', Codes::HTTP_NO_CONTENT);
    }

    private function getMainEntityRepository($em)
    {
        return $em->getRepository('PadelBundle:Reservation');
    }
}