<?php

namespace PadelBundle\Controller;

use FOS\RestBundle\Util\Codes;
use PadelBundle\Entity\Court;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use FOS\RestBundle\Controller\Annotations\View;
use FOS\RestBundle\Controller\Annotations\RouteResource;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

use Nelmio\ApiDocBundle\Annotation\ApiDoc;

/**
 * @RouteResource("Courts")
 */
class CourtsController extends Controller implements SimpleRestfullCrudController
{
    /**
     * @View()
     *
     * @ApiDoc(
     *  description="Return a collection of courts",
     *  output={
     *     "class"="PadelBundle\Entity\Court",
     *     "groups"={"detail_court"},
     *     "parsers"={
     *          "Nelmio\ApiDocBundle\Parser\JmsMetadataParser"
     *      }
     *  }
     * )
     */
    public function cgetAction()
    {
        $em = $this->get('doctrine')->getEntityManager();
        $courts = $this->getMainEntityRepository($em)->findAll();

        return $courts;
    }

    /**
     * @View()
     *
     * @ApiDoc(
     *  description="Return a court",
     *  resource=true,
     *  output={
     *     "class"="PadelBundle\Entity\Court",
     *     "groups"={"detail_court"},
     *     "parsers"={
     *          "Nelmio\ApiDocBundle\Parser\JmsMetadataParser"
     *      }
     *  }
     * )
     */
    public function getAction($slug)
    {
        $em = $this->get('doctrine')->getEntityManager();
        $court = $this->getMainEntityRepository($em)->find($slug);

        return $court;
    }

    /**
     * @View()
     *
     * @ApiDoc(
     *  description="Create a court",
     *  input={
     *    "class"="PadelBundle\Entity\Courts"
     *  },
     *  output={
     *     "class"="PadelBundle\Entity\Court",
     *     "groups"={"detail_court"},
     *     "parsers"={
     *          "Nelmio\ApiDocBundle\Parser\JmsMetadataParser"
     *      }
     *  }
     * )
     */
    public function postAction(Request $request)
    {
        $court = new Court();
        return $this->updateCourt($court, false);
    }

    /**
     * @View()
     *
     * @ApiDoc(
     *  description="Update a court",
     *  input={
     *    "class"="PadelBundle\Entity\Courts"
     *  },
     *  output={
     *     "class"="PadelBundle\Entity\Court",
     *     "groups"={"detail_court"},
     *     "parsers"={
     *          "Nelmio\ApiDocBundle\Parser\JmsMetadataParser"
     *      }
     *  }
     * )
     */
    public function putAction($slug, Request $request)
    {
        $em = $this->get('doctrine')->getEntityManager();
        $court = $this->getMainEntityRepository($em)->find($slug);

        return $this->updateCourt($court, true);
    }

    /**
     * @View()
     *
     * @ApiDoc(
     *  description="Update a court",
     *  input={
     *    "class"="PadelBundle\Entity\Courts"
     *  },
     *  output={
     *     "class"="PadelBundle\Entity\Court",
     *     "groups"={"detail_court"},
     *     "parsers"={
     *          "Nelmio\ApiDocBundle\Parser\JmsMetadataParser"
     *      }
     *  }
     * )
     */
    public function patchAction($slug, Request $request)
    {
        $em = $this->get('doctrine')->getEntityManager();
        $court = $this->getMainEntityRepository($em)->find($slug);

        return $this->updateCourt($court, false);
    }

    /**
     * @View()
     *
     * @ApiDoc(
     *  description="Delete a court",
     *  statusCodes={
     *    204="No content"
     *  }
     * )
     */
    public function deleteAction($slug)
    {
        $em = $this->get('doctrine')->getEntityManager();
        $court = $this->getMainEntityRepository($em)->find($slug);

        if (empty($court)) {
            return new JsonResponse('', Codes::HTTP_NO_CONTENT);
        }

        $em->remove($court);
        $em->flush();

        return new JsonResponse('', Codes::HTTP_NO_CONTENT);
    }

    private function updateCourt($court, $required)
    {
        if (empty($court)) {
            return new JsonResponse('', Codes::HTTP_NO_CONTENT);
        }

        $this->get('api.param_updater')
            ->setEntity($court)
            ->updateFromRequest('active', $required);

        $em = $this->get('doctrine')->getEntityManager();
        $em->persist($court);
        $em->flush();

        return $court;
    }

    private function getMainEntityRepository($em)
    {
        return $em->getRepository('PadelBundle:Court');
    }
}
