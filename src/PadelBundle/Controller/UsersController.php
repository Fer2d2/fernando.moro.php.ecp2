<?php

namespace PadelBundle\Controller;

use PadelBundle\Entity\User;
use PadelBundle\Utils\ApiMessages;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use FOS\RestBundle\Util\Codes;
use FOS\RestBundle\Controller\Annotations\View;
use FOS\RestBundle\Controller\Annotations\RouteResource;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\HttpException;

/**
 * @RouteResource("Users")
 */
class UsersController extends Controller implements SimpleRestfullCrudController
{
    /**
     * @View(serializerGroups={"detail_user", "summary_group"})
     */
    public function cgetAction()
    {
        $em = $this->get('doctrine')->getEntityManager();
        $users = $this->getMainEntityRepository($em)->findAll();

        return $users;
    }

    /**
     * @View(serializerGroups={"detail_user", "summary_group"})
     */
    public function getAction($slug)
    {
        $em = $this->get('doctrine')->getEntityManager();
        $user = $this->getMainEntityRepository($em)->find($slug);

        return $user;
    }

    /**
     * @View(serializerGroups={"detail_user", "summary_group"})
     */
    public function postAction(Request $request)
    {
        $paramUpdater = $this->get('api.param_updater');
        $user = new User();

        $password = password_hash($request->request->get('password'), PASSWORD_DEFAULT);

        // body parameters
        $paramUpdater
            ->setEntity($user)
            ->updateFromRequest('username', true)
            ->updateFromRequest('email', true)
            ->update('password', $password, true)
            ->updateFromRequest('roles', true)
            ->updateFromRequest('enabled', true)
            ->updateFromRequest('locked', true)
            ->updateFromRequest('expired', true);

        // inherited parameters
        $this->generateInheritedParameters($request, $user);

        $em = $this->get('doctrine')->getEntityManager();
        $em->persist($user);

        try {
            $em->flush();
        } catch (\Exception $e) {
            throw new HttpException(400, ApiMessages::ERROR_STORING_VALUE);
        }

        return $user;
    }

    /**
     * @View(serializerGroups={"detail_user", "summary_group"})
     */
    public function putAction($slug, Request $request)
    {
        $paramUpdater = $this->get('api.param_updater');
        $em = $this->get('doctrine')->getEntityManager();

        $user = $this->getMainEntityRepository($em)->find($slug);

        if (empty($user)) {
            return new JsonResponse('', Codes::HTTP_NO_CONTENT);
        }

        $password = password_hash($request->request->get('password'), PASSWORD_DEFAULT);

        // body parameters
        $paramUpdater
            ->setEntity($user)
            ->updateFromRequest('username', true)
            ->updateFromRequest('email', true)
            ->update('password', $password, true)
            ->updateFromRequest('roles', true)
            ->updateFromRequest('enabled', true)
            ->updateFromRequest('locked', true)
            ->updateFromRequest('expired', true);

        // inherited parameters
        $this->generateInheritedParameters($request, $user);

        $em->persist($user);

        try {
            $em->flush();
        } catch (\Exception $e) {
            return new HttpException(400, ApiMessages::ERROR_STORING_VALUE);
        }

        return $user;
    }

    /**
     * @View(serializerGroups={"detail_user", "summary_group"})
     */
    public function patchAction($slug, Request $request)
    {
        $paramUpdater = $this->get('api.param_updater');
        $em = $this->get('doctrine')->getEntityManager();

        $user = $this->getMainEntityRepository($em)->find($slug);

        if (empty($user)) {
            return new JsonResponse('', Codes::HTTP_NO_CONTENT);
        }

        $password = password_hash($request->request->get('password'), PASSWORD_DEFAULT);

        // body parameters
        $paramUpdater
            ->setEntity($user)
            ->updateFromRequest('username', false)
            ->updateFromRequest('email', false)
            ->update('password', $password, false)
            ->updateFromRequest('roles', false)
            ->updateFromRequest('enabled', false)
            ->updateFromRequest('locked', false)
            ->updateFromRequest('expired', false);

        // inherited parameters
        $this->generateInheritedParameters($request, $user);

        $em->persist($user);

        try {
            $em->flush();
        } catch (\Exception $e) {
            return new HttpException(400, ApiMessages::ERROR_STORING_VALUE);
        }

        return $user;
    }

    /**
     * @View()
     */
    public function deleteAction($slug)
    {
        $em = $this->get('doctrine')->getEntityManager();
        $user = $this->getMainEntityRepository($em)->find($slug);

        if (empty($user)) {
            return new JsonResponse('', Codes::HTTP_NO_CONTENT);
        }

        $em->remove($user);
        $em->flush();

        return new JsonResponse('', Codes::HTTP_NO_CONTENT);
    }

    /**
     * @View(serializerGroups={"detail_user", "summary_group"})
     */
    public function postGroupsAction($slug, $id)
    {
        $em = $this->get('doctrine')->getEntityManager();
        $user = $this->getMainEntityRepository($em)->find($slug);
        $group = $em->getRepository('PadelBundle:Group')->find($id);

        if (empty($user) || empty($group)) {
            throw new HttpException(400, ApiMessages::ERROR_ELEMENT_NOT_FOUND);
        }

        if ($user->getGroup()->contains($group)) {
            return $user;
        }

        $user->addGroup($group);

        $em->persist($user);
        $em->flush();

        return $user;
    }

    /**
     * @View()
     */
    public function deleteGroupsAction($slug, $id)
    {
        $em = $this->get('doctrine')->getEntityManager();
        $user = $this->getMainEntityRepository($em)->find($slug);
        $group = $em->getRepository('PadelBundle:User')->find($id);

        if (empty($user) || empty($group)) {
            throw new HttpException(400, ApiMessages::ERROR_ELEMENT_NOT_FOUND);
        }

        if (!$user->getGroup()->contains($group)) {
            return new JsonResponse('', Codes::HTTP_NO_CONTENT);
        }

        $user->removeGroup($group);

        $em->persist($user);
        $em->flush();

        return new JsonResponse('', Codes::HTTP_NO_CONTENT);
    }

    private function getMainEntityRepository($em)
    {
        return $em->getRepository('PadelBundle:User');
    }

    private function generateInheritedParameters(Request $request, $user)
    {
        $paramUpdater = $this->get('api.param_updater');
        $paramUpdater->setEntity($user);

        $username = $request->request->get('username');
        if (isset($username)) {
            $usernameCanonical = strtolower($username);
            $paramUpdater->update('usernameCanonical', $usernameCanonical, true);
        }

        $email = $request->request->get('email');
        if (isset($email)) {
            $emailCanonical = strtolower($email);
            $paramUpdater->update('emailCanonical', $emailCanonical, true);
        }

        $salt = sha1(md5('randomString'));
        $paramUpdater->update('salt', $salt, true);

        $expired = false;
        $paramUpdater->update('credentialsExpired', $expired, true);
    }
}