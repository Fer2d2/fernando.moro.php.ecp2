<?php

namespace PadelBundle\Controller;

use PadelBundle\Entity\Group;
use PadelBundle\Utils\ApiMessages;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use FOS\RestBundle\Util\Codes;
use FOS\RestBundle\Controller\Annotations\View;
use FOS\RestBundle\Controller\Annotations\RouteResource;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\HttpException;

/**
 * @RouteResource("Groups")
 */
class GroupsController extends Controller implements SimpleRestfullCrudController
{
    /**
     * @View(serializerGroups={"detail_group", "summary_user"})
     */
    public function cgetAction()
    {
        $em = $this->get('doctrine')->getEntityManager();
        $groups = $this->getMainEntityRepository($em)->findAll();

        return $groups;
    }

    /**
     * @View(serializerGroups={"detail_group", "summary_user"})
     */
    public function getAction($slug)
    {
        $em = $this->get('doctrine')->getEntityManager();
        $group = $this->getMainEntityRepository($em)->find($slug);

        return $group;
    }

    /**
     * @View(serializerGroups={"detail_group", "summary_user"})
     */
    public function postAction(Request $request)
    {
        $paramUpdater = $this->get('api.param_updater');

        $group = new Group();

        $paramUpdater
            ->setEntity($group)
            ->updateFromRequest('name', true)
            ->updateFromRequest('roles', true);

        $em = $this->get('doctrine')->getEntityManager();
        $em->persist($group);

        try {
            $em->flush();
        } catch (\Exception $e) {
            throw new HttpException(400, ApiMessages::ERROR_STORING_VALUE);
        }

        return $group;
    }

    /**
     * @View(serializerGroups={"detail_group", "summary_user"})
     */
    public function putAction($slug, Request $request)
    {
        $paramUpdater = $this->get('api.param_updater');
        $em = $this->get('doctrine')->getEntityManager();

        $group = $this->getMainEntityRepository($em)->find($slug);

        if (empty($group)) {
            return new JsonResponse('', Codes::HTTP_NO_CONTENT);
        }

        $paramUpdater
            ->setEntity($group)
            ->updateFromRequest('name', true)
            ->updateFromRequest('roles', true);

        $em->persist($group);

        try {
            $em->flush();
        } catch (\Exception $e) {
            return new HttpException(400, ApiMessages::ERROR_STORING_VALUE);
        }

        return $group;
    }

    /**
     * @View()
     */
    public function deleteAction($slug)
    {
        $em = $this->get('doctrine')->getEntityManager();
        $group = $this->getMainEntityRepository($em)->find($slug);

        if (empty($group)) {
            return new JsonResponse('', Codes::HTTP_NO_CONTENT);
        }

        $em->remove($group);
        $em->flush();

        return new JsonResponse('', Codes::HTTP_NO_CONTENT);
    }

    /**
     * @View(serializerGroups={"detail_group", "summary_user"})
     */
    public function patchAction($slug, Request $request)
    {
        $paramUpdater = $this->get('api.param_updater');
        $em = $this->get('doctrine')->getEntityManager();

        $group = $this->getMainEntityRepository($em)->find($slug);

        if (empty($group)) {
            return new JsonResponse('', Codes::HTTP_NO_CONTENT);
        }

        $paramUpdater
            ->setEntity($group)
            ->updateFromRequest('name', false)
            ->updateFromRequest('roles', false);

        $em->persist($group);

        try {
            $em->flush();
        } catch (\Exception $e) {
            return new HttpException(400, ApiMessages::ERROR_STORING_VALUE);
        }

        return $group;
    }

    /**
     * @View(serializerGroups={"detail_group", "summary_user"})
     */
    public function postUsersAction($slug, $id)
    {
        $em = $this->get('doctrine')->getEntityManager();
        $group = $this->getMainEntityRepository($em)->find($slug);
        $user = $em->getRepository('PadelBundle:User')->find($id);

        if (empty($group) || empty($user)) {
            throw new HttpException(400, ApiMessages::ERROR_ELEMENT_NOT_FOUND);
        }

        if ($group->getUser()->contains($user)) {
            return $group;
        }

        $group->addUser($user);

        $em->persist($group);
        $em->flush();

        return $group;
    }

    /**
     * @View()
     */
    public function deleteUsersAction($slug, $id)
    {
        $em = $this->get('doctrine')->getEntityManager();
        $group = $this->getMainEntityRepository($em)->find($slug);
        $user = $em->getRepository('PadelBundle:User')->find($id);

        if (empty($group) || empty($user)) {
            throw new HttpException(400, ApiMessages::ERROR_ELEMENT_NOT_FOUND);
        }

        if (!$group->getUser()->contains($user)) {
            return new JsonResponse('', Codes::HTTP_NO_CONTENT);
        }

        $group->removeUser($user);

        $em->persist($group);
        $em->flush();

        return new JsonResponse('', Codes::HTTP_NO_CONTENT);
    }

    private function getMainEntityRepository($em)
    {
        return $em->getRepository('PadelBundle:Group');
    }
}