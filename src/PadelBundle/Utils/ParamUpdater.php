<?php

namespace PadelBundle\Utils;

use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\PropertyAccess\PropertyAccess;

class ParamUpdater
{
    private $requestStack;
    private $entity;

    public function __construct(RequestStack $requestStack)
    {
        $this->requestStack = $requestStack;
    }

    public function update($parameter_name, $parameter_value, $required)
    {
        $accessor = PropertyAccess::createPropertyAccessorBuilder()
            ->getPropertyAccessor();

        if ($required) {
            if (!isset($parameter_value)) {
                throw new HttpException(400, "patameter $parameter_name was not found");
            }
        }

        if (isset($parameter_value)) {
            $accessor->setValue($this->entity, $parameter_name, $parameter_value);
        }

        return $this;
    }

    public function updateFromRequest($parameter_name, $required)
    {
        $parameter_value = $this->requestStack->getCurrentRequest()->request->get($parameter_name);
        $this->update($parameter_name, $parameter_value, $required);
        return $this;
    }

    public function setEntity($entity)
    {
        $this->entity = $entity;
        return $this;
    }
}