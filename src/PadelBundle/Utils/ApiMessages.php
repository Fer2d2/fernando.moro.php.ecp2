<?php

namespace PadelBundle\Utils;


abstract class ApiMessages
{
    const ERROR_STORING_VALUE = "Error storing values";
    const ERROR_ELEMENT_NOT_FOUND = "Element was not found";

}